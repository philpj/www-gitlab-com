---
layout: markdown_page
title: "GitLab Partners"
---
## GitLab Strategic Partners

GitLab strategic partners are large partnerships in key areas. 

### Google
Seamless experience with GKE 

GitLab.com on [Google Cloud Platform](/google-cloud-platform/)

Google partner award winner - innovative solution developer ecosystem
 

### IBM
GitLab Core running on IBM Cloud Public - SCM integrating with their DevOps toolchain

https://console.bluemix.net/devops/getting-started 

https://www.ibm.com/cloud//continuous-delivery

### AWS
[EKS support](/2018/06/06/eks-gitlab-integration/)
