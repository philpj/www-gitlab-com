module Gitlab
  module Homepage
    class Team
      class Member
        attr_reader :assignments

        def initialize(data)
          @data = data
          @assignments = []
        end

        def anchor
          twitter || gitlab || name.parameterize
        end

        def username
          @data.fetch('gitlab')
        end

        def involved?(project)
          roles.has_key?(project.key)
        end

        def roles
          @roles ||= {}.tap do |hash|
            @data.fetch('projects', {}).each do |project, roles|
              Array(roles).each { |role| (hash[project] ||= []) << role }
            end
          end
        end

        def assign(project)
          roles[project.key].each do |role|
            @assignments << Team::Assignment.new(self, project, role)
          end
        end

        def departments
          @departments ||= @data.fetch('departments', [])
        end

        def country_normalized
          country = if @data['country'] == 'USA'
                      'United States'
                    elsif @data['country'] == 'The Netherlands'
                      'Netherlands'
                    else
                      @data['country']
                    end

          country
        end

        ##
        # Middeman Data File objects compatibiltiy
        #
        def method_missing(name, *args, &block) # rubocop:disable Style/MethodMissing
          @data[name.to_s]
        end

        def self.all!
          @members ||= YAML.load_file('data/team.yml')
          @titles ||= {}.tap do |hash|
            @members.each do |member|
              hash[member['slug']] = member['role'] if member['slug']
            end
          end

          @members.map do |data|
            reports_to = data['reports_to']
            data['reports_to_title'] = @titles[reports_to] if reports_to
            new(data).tap { |member| yield member if block_given? }
          end
        end
      end
    end
  end
end
